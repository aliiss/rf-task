import matplotlib.pyplot as plt
import numpy as np

fmin = 0.1
fmax = 7.0
#lvl: +15 dBm
num = 401
x = np.linspace(fmin, fmax, num, endpoint=True)


def load(name):
    with open(f"{name}_R.dat", "r") as f:
        lines = f.readlines()
        y = []
        for line in lines:
            value = float(line)
            y.append(value)
        y_R = np.array(y)
    with open(f"{name}_T.dat", "r") as f:
        lines = f.readlines()
        y = []
        for line in lines:
            value = float(line)
            y.append(value)
        y_T = np.array(y)
    return y_R, y_T        
        

def convert(fname, frq_Hz, lvl_dB, label):
    with open(f".\\{fname}.csv", "w") as f:
        f.write(f'Freq[Hz], {label}[dB]\n')
        for i1, i2 in zip(frq_Hz, lvl_dB):
            f.write(f"{i1:.0f}, {i2:.4f}\n")
      
title = 'Khune_LNA_1_S11_S21_-25dBin'
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
        
yr, yt = load(f'{title}')
ax.plot(x,yr, label='S11')        
ax.plot(x,yt, label='S21')
convert('Khune_LNA_1_-25dBin_S11', x*1e9, yr, 'S11')
convert('Khune_LNA_1_-25dBin_S21', x*1e9, yt, 'S21')

ax.set_xlabel('Frequency (GHz)', fontsize=8)
ax.set_ylabel('S-parameter (dB)', fontsize=8)
ax.legend(loc='lower right', fontsize=6)
ax.grid(visible='True')
ax.set_ylim(-70, 35)
height = 6.5 # cm 
width = 13.89 # 17.79 # cm
fig.set_size_inches(width/2.54, height/2.54)
plt.savefig(f'.\\{title}.png', dpi=600, bbox_inches='tight', transparent=False)
plt.show()