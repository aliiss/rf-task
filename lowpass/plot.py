# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 11:43:54 2023

@author: sepo
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

def load(name):
    f = open(name, "r")
    lines = f.readlines()
    f.close()
    y1 = []
    for indx, line in enumerate(lines):
        y1.append(float(line))
    x1 = np.linspace(0.01, 10, len(y1), endpoint=True)
    maxS21_1 = max(y1)
    return np.array(x1), np.array(y1)-maxS21_1


x1, y1 = load("dB_lowpass0-10_LowPass6GHz_2.dat")

fig = plt.figure()
font = {'family' : 'sans-serif', 'size'   : 7}  #'weight' : 'bold',
rc('font', **font)
ax = fig.add_subplot(1,1,1)
ax.plot(x1, y1, label="LPF")
xticks = np.arange(0, 10, 1)
yticks = np.arange(-100, 0, 10)
ax.set_xlabel('Frequency (GHz)')
ax.set_ylabel('$S_{21}$ (dB)')
#ax.plot(x, -3, '--', color = 'gray')
#ax.plot(x, yy, label='S21(DEG)')
ax.legend(loc='best', fontsize=6)
ax.grid(visible='True')
#ax.set_xticks(xticks)
#ax.set_yticks(yticks)
#ax.set_xlim(173.8, 174.04)
ax.set_xlim(0, 10)
ax.set_ylim(-40, 0)
#ax.set_aspect('equal')
height = 6.0 # cm 
width = 8.89 # 17.79 # cm
fig.set_size_inches(width/2.54, height/2.54)
plt.savefig('.\\lpf_s21.png', dpi=600, bbox_inches='tight', transparent=False)
plt.show()


print("LPF:")
maxS21_1 = max(y1)
setval, meany = [], []
for ix, iy in zip(x1, y1):
    if iy >= maxS21_1 - 3:
        setval.append(ix)
        meany.append(iy)
print(min(setval), max(setval), np.mean(meany) )
