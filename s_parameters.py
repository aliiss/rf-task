import numpy as np
from scipy import interpolate

# compute S parameters for a two-device chain

def load_S_param(name, from_line=7, inx_frq=0, idx_lvl=1):
    f = open(name, "r")
    lines = f.readlines()
    f.close()
    x1, y1 = [], []
    for indx, line in enumerate(lines):
        if indx >= from_line:
            try:
                d = line.split(',')
                x1.append(float(d[inx_frq])/1e9)               

                if float(d[1]) >= 0:
                    y1.append(float(-0.2))
                else:
                    y1.append( float(d[idx_lvl]) )  
            except:
                pass
    return np.array(x1), np.array(y1)

def interpolate_S_param(xx, yy, startFreq, stopFreq, points):
    function = interpolate.interp1d(xx, yy)
    freqSet = np.linspace(startFreq, stopFreq, num=points, endpoint=True, dtype=float)
    yy_int = function(freqSet)
    return freqSet, yy_int

def calculate_chain(S11_a, S21_a, S12_a, S22_a, S11_b, S21_b, S12_b, S22_b):
    fS11_a = np.power(10, S11_a/10)
    fS12_a = np.power(10, S12_a/10)
    fS21_a = np.power(10, S21_a/10)
    fS22_a = np.power(10, S22_a/10)
    fS11_b = np.power(10, S11_b/10)
    fS12_b = np.power(10, S12_b/10)
    fS21_b = np.power(10, S21_b/10)
    fS22_b = np.power(10, S22_b/10)
        
    fS11 = fS11_a + fS12_a*fS11_b*fS21_a / (1 - fS11_b * fS22_a)
    fS12 = fS12_a*fS12_b / (1 - fS11_b * fS22_a)
    fS21 = fS21_b*fS21_a / (1 - fS11_b * fS22_a)
    fS22 = fS22_b + fS21_b*fS22_a*fS12_b / (1 - fS11_b * fS22_a)
    return 10*np.log10(fS11), 10*np.log10(fS21), 10*np.log10(fS12), 10*np.log10(fS22)


if __name__ == "__main__":
    pass   