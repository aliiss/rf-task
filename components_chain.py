import matplotlib.pyplot as plt
from matplotlib import rc
from s_parameters import load_S_param, interpolate_S_param, calculate_chain

origin = '.' 
    
# SUHNER DC-Block
name = origin + '/dc_block/dc_block_SUHNER/Dc_block_SUHNER_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_0 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/dc_block/dc_block_SUHNER/Dc_block_SUHNER_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_0 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/dc_block/dc_block_SUHNER/Dc_block_SUHNER_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_0 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/dc_block/dc_block_SUHNER/Dc_block_SUHNER_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_0 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# Limiter 30-6000 MHz
name = origin + '/limiter_minicircuits/Limiter_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_1 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/limiter_minicircuits/Limiter_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_1 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/limiter_minicircuits/Limiter_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_1 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/limiter_minicircuits/Limiter_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_1 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# DEEWAVE 42601
name = origin + '/IF_stage_Isolator/deewave_42601/Deewave_42601_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_2 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/IF_stage_Isolator/deewave_42601/Deewave_42601_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_2 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/IF_stage_Isolator/deewave_42601/Deewave_42601_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_2 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/IF_stage_Isolator/deewave_42601/Deewave_42601_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_2 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# DC Block (white)
name = origin + '/dc_block/Dc_block_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_3 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/dc_block/Dc_block_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_3 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/dc_block/Dc_block_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_3 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/dc_block/Dc_block_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_3 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# Brown cable 
name = origin + '/coax_brown/Coax_brown_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_4 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/coax_brown/Coax_brown_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_4 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/coax_brown/Coax_brown_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_4 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/coax_brown/Coax_brown_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_4 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# LPF
name = origin + '/lowpass/LPF_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_5 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/lowpass/LPF_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_5 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/lowpass/LPF_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_5 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/lowpass/LPF_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_6 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# Coax blue
name = origin + '/coax_blue/Coax_blue_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_6 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/coax_blue/Coax_blue_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_6 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/coax_blue/Coax_blue_S12.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S12_6 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/coax_blue/Coax_blue_S22.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S22_6 = interpolate_S_param(x, y, 0.15, 6.5, 401)

# LNA @ -15 dB in 
name = origin + '/1/-15dBin/Khune_LNA_1_-15dBin_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_7 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/1/-15dBin/Khune_LNA_1_-15dBin_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_7 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/1/-15dBin/Khune_LNA_1_-15dBin_S11.csv' # STAND IN
x, y = load_S_param(name=name, from_line=1)
ff, S12_7 = interpolate_S_param(x, y, 0.15, 6.5, 401) 

name = origin + '/1/-15dBin/Khune_LNA_1_-15dBin_S21.csv' # STAND IN
x, y = load_S_param(name=name, from_line=1)
ff, S22_7 = interpolate_S_param(x, y, 0.15, 6.5, 401) 

# LNA @ 0 dB in 
name = origin + '/1/0dBin/Khune_LNA_1_0dBin_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_8 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/1/0dBin/Khune_LNA_1_0dBin_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_8 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/1/0dBin/Khune_LNA_1_0dBin_S21.csv' # STAND IN
x, y = load_S_param(name=name, from_line=1)
ff, S12_8 = interpolate_S_param(x, y, 0.15, 6.5, 401) 

name = origin + '/1/0dBin/Khune_LNA_1_0dBin_S21.csv' # STAND IN
x, y = load_S_param(name=name, from_line=1)
ff, S22_8 = interpolate_S_param(x, y, 0.15, 6.5, 401) 

# LNA @ -5 dB in 
name = origin + '/1/-5dBin/Khune_LNA_1_-5dBin_S11.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S11_9 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/1/-5dBin/Khune_LNA_1_-5dBin_S21.csv'
x, y = load_S_param(name=name, from_line=1)
ff, S21_9 = interpolate_S_param(x, y, 0.15, 6.5, 401)

name = origin + '/1/-5dBin/Khune_LNA_1_-5dBin_S21.csv' # STAND IN
x, y = load_S_param(name=name, from_line=1)
ff, S12_9 = interpolate_S_param(x, y, 0.15, 6.5, 401) 

name = origin + '/1/-5dBin/Khune_LNA_1_-5dBin_S21.csv' # STAND IN
x, y = load_S_param(name=name, from_line=1)
ff, S22_9 = interpolate_S_param(x, y, 0.15, 6.5, 401) 



##################
# 0 -> Suhner DC block
# 1 -> limiter
# 2 -> Dwave isolator
# 3 -> DC Block (white)
# 4 -> Brown coax
# 5 -> LPF
# 6 -> Coax blue
# 7 -> LNA @ 15 dBin
# 8 -> LNA @ 0 dBin


## a. (SUHNER DC-Block)=>(Limiter 30-6000 MHz)=>(DEEWAVE 42601)
# chain_params = calculate_chain(S11_0, S21_0, S12_0, S22_0, S11_1, S21_1, S12_1, S22_1)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_2, S21_2, S12_2, S22_2)

## b. Limiter -> DWAVE -> DC block
# chain_params = calculate_chain(S11_1, S21_1, S12_1, S22_1, S11_2, S21_2, S12_2, S22_2) #limiter to Dwave
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_0, S21_0, S12_0, S22_0)

## c. Limiter
# chain_params = [S11_1, S21_1, S12_1, S22_1]

## d. Limiter -> DC block (white)
# chain_params = calculate_chain(S11_1, S21_1, S12_1, S22_1, S11_3, S21_3, S12_3, S22_3)

## e. Limiter -> DWAVE -> DC block (white)
# chain_params = calculate_chain(S11_1, S21_1, S12_1, S22_1, S11_2, S21_2, S12_2, S22_2)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)

## f. DC block (white) -> limiter
# chain_params = calculate_chain(S11_3, S21_3, S12_3, S22_3, S11_1, S21_1, S12_1, S22_1)

## g. DWAVE → limiter → brown cable → dc block
# chain_params = calculate_chain(S11_2, S21_2, S12_2, S22_2, S11_1, S21_1, S12_1, S22_1)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_4, S21_4, S12_4, S22_4)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)

## h. DWAVE -> LPF -> Limiter -> Brown cable -> DC block
# chain_params = calculate_chain(S11_2, S21_2, S12_2, S22_2, S11_5, S21_5, S12_5, S22_5)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_1, S21_1, S12_1, S22_1)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_4, S21_4, S12_4, S22_4)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)

# i. Limiter -> Brown cable -> DC block
# chain_params = calculate_chain(S11_1, S21_1, S12_1, S22_1, S11_4, S21_4, S12_4, S22_4)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)

# j. D-Wave → limiter → Brown cable → DC block
# chain_params = calculate_chain(S11_2, S21_2, S12_2, S22_2, S11_1, S21_1, S12_1, S22_1)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_4, S21_4, S12_4, S22_4)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)

# # k. Blue cable → Dwave → LNA(-15 dBin) → brown cable → dc block 
# chain_params = calculate_chain(S11_6, S21_6, S12_6, S22_6, S11_2, S21_2, S12_2, S22_2)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_7, S21_7, S12_7, S22_7)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_4, S21_4, S12_4, S22_4)
# chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)

# k. Blue cable → Dwave → LNA(0 dBin) → brown cable → dc block 
chain_params = calculate_chain(S11_6, S21_6, S12_6, S22_6, S11_2, S21_2, S12_2, S22_2)
chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_9, S21_9, S12_9, S22_9)
chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_4, S21_4, S12_4, S22_4)
chain_params = calculate_chain(chain_params[0], chain_params[1], chain_params[2], chain_params[3], S11_3, S21_3, S12_3, S22_3)


fig = plt.figure()
font = {'family' : 'sans-serif', 'size'   : 7}  #'weight' : 'bold',
rc('font', **font)
ax = fig.add_subplot(1,1,1)

ax.plot(ff, chain_params[0] - 23, "--", label="S11")
ax.plot(ff, chain_params[1] - 23, "--", label="S21")
# ax.plot(ff, chain_params[2], "--", label="S12")
# ax.plot(ff, chain_params[3], "--", label="S22")

#ax.plot(ff, S21_a+S21_b, "--", label="21 sum")


# xticks = np.arange(137, 180, 3)
# yticks = np.arange(-100, 0.1, 10)

ax.set_xlabel('Frequency (GHz)')
ax.set_ylabel('$S_{11}$ (dB)')

#ax.plot(x, -3, '--', color = 'gray')
#ax.plot(x, yy, label='S21(DEG)')
ax.legend(loc='best', fontsize=6)
ax.grid(visible='True')
#ax.set_xticks(xticks)
#ax.set_yticks(yticks)
#ax.set_xlim(173.8, 174.04)
#ax.set_xlim(137, 180)
# ax.set_ylim(-200, 10)
#ax.set_aspect('equal')
height = 6.0 # cm 
width = 8.89 # 17.79 # cm
fig.set_size_inches(width/2.54, height/2.54)
plt.savefig('plot.png', dpi=600, bbox_inches='tight', transparent=False)
plt.show()